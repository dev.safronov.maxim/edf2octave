function [signals] = edf2matlab(filename)
   #clear
   edf_file = fopen(filename);
   edf_file = fopen('pat00001.edf');
   edf_header = fread(edf_file, [1,256], 'char'); #читаем заголовок (256 байт)
   if (edf_header(1, 1) != 48)
     print('File is not a valid EDF');
     return;
   endif
   patient_id = edf_header(1, 9:88); #читаем id пациента (80 байт)
   #printf"%s",(edf_header(1, 169:169+8))
   startdate = char(edf_header(1, 169:176)); #читаем дату начала чаписи (8 байт)
   starttime = char(edf_header(1, 177:184)); #читаем время начала чаписи (8 байт)
   number_of_bytes_in_header = str2double(char(edf_header(1, 185:191))); #читаем количество байт в заголовке (8 байт)
   number_of_data_records = str2double(char(edf_header(1, 236:243))); #читаем количество записей в файле
   duration_of_data_record = str2double(char(edf_header(1, 244:251)));#читаем количетсво секунд в записи
   number_of_signals = str2double(char(edf_header(1, 252:256))); #читаем количество сигналов
   
   #############################################################################
   #             Далее начинается чтение всех заголовков данных                #
   #############################################################################
   
   edf_data_header = fread(edf_file, [1, number_of_bytes_in_header - 256], 'char'); #читаем оставшую часть заголовка с инфой о данных
   #читаем названия
   labels = [number_of_signals, blanks(15)];
   for header = 16:16:16*number_of_signals
     labels(header / 16, 1:16) = char(edf_data_header(1, header-15:header));
   end
   #читаем transducer
   transducer = char(edf_data_header(1, 16:80*number_of_signals));
   #читаем единицы измерения сигналов
   physical_dims_shift = number_of_signals * (16 + 80); #сдвиг курсора для чтения единиц измерений
   physical_dims = [number_of_signals, blanks(7)]; 
   for header = 8:8:8*number_of_signals
     physical_dims(header / 8, 1:8) = char(edf_data_header(1, physical_dims_shift + header-7: physical_dims_shift + header));
   end
   #читаем минимумы сигналов
   physical_mins_shift = number_of_signals * (16 + 80 + 8); #сдвиг курсора для чтения минимальных значений
   for header = 8:8:8*number_of_signals
     physical_mins(header / 8) = str2double(char(edf_data_header(1, physical_mins_shift + header-7: physical_mins_shift + header)));
   end
   #также максимумы
   physical_maxs_shift = number_of_signals * (16 + 80 + 8 + 8);
   for header = 8:8:8*number_of_signals
     physical_maxs(header / 8) = str2double(char(edf_data_header(1, physical_maxs_shift + header-7: physical_maxs_shift + header)));
   end
   digital_mins_shift = number_of_signals * (16 + 80 + 8 + 8 + 8);
   for header = 8:8:8*number_of_signals
     digital_mins(header / 8) = str2double(char(edf_data_header(1, digital_mins_shift + header-7: digital_mins_shift + header)));
   end
   digital_maxs_shift = number_of_signals * (16 + 80 + 8 + 8 + 8 + 8);
   for header = 8:8:8*number_of_signals
     digital_maxs(header / 8) = str2double(char(edf_data_header(1, digital_maxs_shift + header-7: digital_maxs_shift + header)));
   end
   #prefiltering
   prefilterings_shift = number_of_signals * (16 + 80 + 8 + 8 + 8 + 8 + 8);
   prefilterings = [number_of_signals, blanks(79)];
   for header = 80:80:80*number_of_signals
     prefilterings(header / 80, 1:80) = char(edf_data_header(1, prefilterings_shift + header-79: prefilterings_shift + header));
   end
   #Читаем количество сэмплов каждой записи
   numbers_of_samples_shift = number_of_signals * (16 + 80 + 8 + 8 + 8 + 8 + 8 + 80);
   for header = 8:8:8*number_of_signals
     numbers_of_samples(header / 8) = str2double(char(edf_data_header(1, numbers_of_samples_shift + header-7: numbers_of_samples_shift + header)));
   end
   
   #############################################################################
   #                    Начинается чтение самих записей                        #
   #############################################################################
   
   signal_data = fread(edf_file, 'int16'); #читаем данные сигналов из файла
   for number = 1:1:number_of_signals
     signals(number, 1:numbers_of_samples(number)) = signal_data(numbers_of_samples(number) * number - (numbers_of_samples(number) - 1): number * numbers_of_samples(number));
   end
endfunction